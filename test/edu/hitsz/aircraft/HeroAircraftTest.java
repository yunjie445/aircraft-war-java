package edu.hitsz.aircraft;

import edu.hitsz.bullet.BaseBullet;
import edu.hitsz.bullet.HeroBullet;
import org.junit.jupiter.api.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Black Box Testing for HeroAircraft")
class HeroAircraftBlackBoxTest {

    private HeroAircraft heroAircraft;

    @BeforeAll
    static void setUpBeforeAll() {
        System.out.println("**--- Black Box Testing for HeroAircraft ---**");
    }

    @BeforeEach
    void setUp() {
        System.out.println("**--- Executed before each test method in this class ---**");
        heroAircraft = HeroAircraft.getInstance();
    }

    @AfterEach
    void tearDown() {
        System.out.println("**--- Executed after each test method in this class ---**");
        heroAircraft = null;
    }

    @DisplayName("Test setShootNum method")
    @Test
    void SetShootNum() {
        System.out.println("**--- Test setShootNum method ---**");
        int num = 3;
        heroAircraft.setShootNum(num);
        assertEquals(num, heroAircraft.getShootNum());
    }

    @Test
    @DisplayName("Test shoot method")
    void shoot() {
        System.out.println("**--- Test shoot method ---**");
        // 调用被测试方法，生成子弹
        Collection<? extends BaseBullet> bullets = heroAircraft.shoot();

        // 验证生成子弹数量是否正确
        assertEquals(heroAircraft.getShootNum(), bullets.size());

        // 验证生成的子弹是否为英雄子弹
        for (BaseBullet bullet : bullets) {
            assertTrue(bullet instanceof HeroBullet);
        }

        // 验证子弹的位置是否在预期范围内
        int expectedX = heroAircraft.getLocationX(); // 预期生成子弹的 X 坐标为英雄飞机的 X 坐标
        int expectedY = heroAircraft.getLocationY() + 2 * -1; // 预期生成子弹的 Y 坐标为英雄飞机的 Y 坐标加上方向乘以 2

        // 创建 X 坐标列表，用于逐个对比
        List<Integer> expectedXList = new ArrayList<>();
        for (int i = 0; i < heroAircraft.getShootNum(); i++) {
            expectedXList.add(expectedX + (i * 2 - heroAircraft.getShootNum() + 1) * 10);
        }

        // 逐个对比子弹的位置
        int index = 0;
        for (BaseBullet bullet : bullets) {
            assertEquals(expectedXList.get(index), bullet.getLocationX());
            assertEquals(expectedY, bullet.getLocationY());

            index++;
        }
    }


    @DisplayName("Test getInstance method")
    @Test
    void GetInstance() {
        System.out.println("**--- Test getInstance method ---**");
        HeroAircraft anotherAircraft = HeroAircraft.getInstance();
        assertSame(heroAircraft, anotherAircraft);
    }
}
