package edu.hitsz.aircraft;

import edu.hitsz.bullet.BaseBullet;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AbstractAircraftTest {

    private AbstractAircraft aircraft;

    @BeforeAll
    static void setUpBeforeAll() {
        System.out.println("**--- Testing for AbstractAircraft ---**");
    }

    @BeforeEach
    void setUp() {

        // 创建一个测试飞机，位置为 (100, 200)，速度为 (0, 0)，生命值为 100
        aircraft = new MockAircraft(100, 200, 0, 0, 100);
    }

    @Test
    @DisplayName("Test decreaseHp method")
    void testDecreaseHp() {
        System.out.println("**--- Test decreaseHp method ---**");
        // 减少飞机的生命值为 20
        aircraft.decreaseHp(20);
        // 验证飞机的生命值是否减少正确
        assertEquals(80, aircraft.getHp());
    }

    @Test
    @DisplayName("Test increaseHp method")
    void testIncreaseHp() {
        System.out.println("**--- Test increaseHp method ---**");
        aircraft.decreaseHp(50);
        // 增加飞机的生命值为 30
        aircraft.increaseHp(30);
        // 验证飞机的生命值是否增加正确
        assertEquals(80, aircraft.getHp());
        aircraft.increaseHp(30);
        assertEquals(100, aircraft.getHp());

    }


    // 模拟的飞机类，用于测试
    private static class MockAircraft extends AbstractAircraft {
        public MockAircraft(int locationX, int locationY, int speedX, int speedY, int hp) {
            super(locationX, locationY, speedX, speedY, hp);
        }

        @Override
        public List<BaseBullet> shoot() {
            return super.shoot();
        }
    }
}
