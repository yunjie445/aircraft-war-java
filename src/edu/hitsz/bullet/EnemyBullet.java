package edu.hitsz.bullet;

import edu.hitsz.prop.BombPropObserver;

public class EnemyBullet extends BaseBullet implements BombPropObserver {

    public EnemyBullet(int locationX, int locationY, int speedX, int speedY, int power) {
        super(locationX, locationY, speedX, speedY, power);
    }

    @Override
    public void bombPropNotified() {
        vanish();
    }
}
