package edu.hitsz.music;

public class MusicManager {

    private static MusicThread bgmPlayer;
    private static MusicThread bossBgmPlayer;
    private static boolean musicOn = false;

    public static void setMusicOn(boolean musicOn) {
        MusicManager.musicOn = musicOn;
    }

    public static void playBgm() {
        if (musicOn) {
            bgmPlayer = new MusicThread("src/videos/bgm.wav", true);
            bgmPlayer.start();
        }
    }

    // 播放BOSS机音乐
    public static void playBossBgm() {
        if (musicOn) {
            bossBgmPlayer = new MusicThread("src/videos/bgm_boss.wav", true);
            bossBgmPlayer.start();
        }
    }

    // 子弹击打声音
    public static void playBulletHit() {
        if (musicOn) {
            new MusicThread("src/videos/bullet_hit.wav", false).start();
        }
    }

    // 播放补血道具音乐
    public static void playSupply() {
        if (musicOn) {
            new MusicThread("src/videos/get_supply.wav", false).start();
        }
    }

    // 播放炸弹道具音乐
    public static void playBomb() {
        if (musicOn) {
            new MusicThread("src/videos/bomb_explosion.wav", false).start();
        }
    }

    // 播放火力道具音乐
    public static void playBullet() {
        if (musicOn) {
            new MusicThread("src/videos/bullet.wav", false).start();
        }
    }

    // 播放游戏结束音乐
    public static void playOverBgm() {
        if (musicOn) {
            new MusicThread("src/videos/game_over.wav", false).start();
        }
    }

    // 停止播放BGM
    public static void overBgm() {
        if (musicOn && bgmPlayer != null) {
            bgmPlayer.over();
        }
    }

    // 停止播放Boss BGM
    public static void overBossBgm() {
        if (musicOn && bossBgmPlayer != null) {
            bossBgmPlayer.over();
        }
    }
}
