package edu.hitsz.enemyfactory;

import edu.hitsz.aircraft.EnemyAircraft;

public abstract class EnemyFactory {

    private int hp;

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public abstract EnemyAircraft createEnemyAircraft();
}
