package edu.hitsz.enemyfactory;

import edu.hitsz.aircraft.ElitePlusEnemy;
import edu.hitsz.application.ImageManager;
import edu.hitsz.application.Main;

public class ElitePlusEnemyFactory extends EnemyFactory {

    public ElitePlusEnemyFactory() {
        setHp(50);
    }

    @Override
    public ElitePlusEnemy createEnemyAircraft() {
        return new ElitePlusEnemy(
                (int) (Math.random() * (Main.WINDOW_WIDTH - ImageManager.MOB_ENEMY_IMAGE.getWidth())),
                (int) (Math.random() * Main.WINDOW_HEIGHT * 0.05),
                1,
                2,
                getHp()
        );
    }
}
