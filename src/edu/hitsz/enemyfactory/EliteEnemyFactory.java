package edu.hitsz.enemyfactory;

import edu.hitsz.aircraft.EliteEnemy;
import edu.hitsz.application.ImageManager;
import edu.hitsz.application.Main;

public class EliteEnemyFactory extends EnemyFactory {

    public EliteEnemyFactory() {
        setHp(30);
    }

    @Override
    public EliteEnemy createEnemyAircraft() {
        return new EliteEnemy(
                (int) (Math.random() * (Main.WINDOW_WIDTH - ImageManager.MOB_ENEMY_IMAGE.getWidth())),
                (int) (Math.random() * Main.WINDOW_HEIGHT * 0.05),
                1,
                2,
                getHp()
        );
    }
}
