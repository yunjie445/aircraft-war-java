package edu.hitsz.application;

import edu.hitsz.aircraft.EnemyAircraft;

public class EasyGame extends Game {
    public EasyGame(boolean musicOn) {
        super(musicOn, 1, 0);
    }

    @Override
    protected void increaseGameDifficulty() {
        // 简单模式不会随时间增长提升游戏难度，重写为空
    }

    @Override
    protected void createEnemyAction() { // 简单模式没有Boss
        createEnemyCycleTime += timeInterval;
        if (enemyAircrafts.size() >= maxEnemyNum || createEnemyCycleTime < createEnemyCycleDuration) {
            return;
        }
        createEnemyCycleTime %= createEnemyCycleDuration;
        EnemyAircraft enemy;
        if (Math.random() < createEliteEnemyProbability) {
            enemy = eliteEnemyFactory.createEnemyAircraft();
        } else {
            enemy = mobEnemyFactory.createEnemyAircraft();
        }
        enemyAircrafts.add(enemy);
        bombPropObservers.add(enemy);
        superEliteAircraftCycleTime += timeInterval;
        if (superEliteAircraftCycleTime >= 2000) { // 产生超级精英敌机的时间间隔
            enemy = elitePlusEnemyFactory.createEnemyAircraft();
            enemyAircrafts.add(enemy);
            bombPropObservers.add(enemy);
            superEliteAircraftCycleTime = 0;
        }
    }
}
