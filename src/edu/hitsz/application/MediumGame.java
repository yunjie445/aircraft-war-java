package edu.hitsz.application;

import edu.hitsz.aircraft.EnemyAircraft;
import edu.hitsz.music.MusicManager;

public class MediumGame extends Game {
    public MediumGame(boolean musicOn) {
        super(musicOn, 2, 12000);
    }

    @Override
    protected void increaseGameDifficulty() {
        if (increaseGameDifficultyCycleTime >= increaseGameDifficultyCycleDuration) {
            increaseGameDifficultyCycleTime %= increaseGameDifficultyCycleDuration;
            if (maxEnemyNum < 8) {
                maxEnemyNum++;
            }
            if (createEnemyCycleDuration > 250) {
                createEnemyCycleDuration -= 30;
            }
            if (createEliteEnemyProbability < 1) {
                createEliteEnemyProbability += 0.1F;
            }
            if (enemyShootCycleDurantion > 300) {
                enemyShootCycleDurantion -= 30;
            }
            // 还可以添加其他难度提升设定
        }
    }

    @Override
    protected void createEnemyAction() {
        createEnemyCycleTime += timeInterval;
        if (enemyAircrafts.size() >= maxEnemyNum || createEnemyCycleTime < createEnemyCycleDuration) {
            return;
        }
        createEnemyCycleTime %= createEnemyCycleDuration;
        EnemyAircraft enemy;
        if (Math.random() < createEliteEnemyProbability) {
            enemy = eliteEnemyFactory.createEnemyAircraft();
        } else {
            enemy = mobEnemyFactory.createEnemyAircraft();
        }
        enemyAircrafts.add(enemy);
        bombPropObservers.add(enemy);
        if (score / 1000 > numOfBossCreated && !isBossExist) {
            enemyAircrafts.add(bossEnemyFactory.createEnemyAircraft());
            isBossExist = true;
            numOfBossCreated++;
            MusicManager.overBgm();
            MusicManager.playBossBgm();
        }
        superEliteAircraftCycleTime += timeInterval;
        if (superEliteAircraftCycleTime >= 2000) { // 产生超级精英敌机的时间间隔
            enemy = elitePlusEnemyFactory.createEnemyAircraft();
            enemyAircrafts.add(enemy);
            bombPropObservers.add(enemy);
            superEliteAircraftCycleTime = 0;
        }
    }
}
