package edu.hitsz.application;

import edu.hitsz.aircraft.AbstractAircraft;
import edu.hitsz.aircraft.BossEnemy;
import edu.hitsz.aircraft.EnemyAircraft;
import edu.hitsz.aircraft.HeroAircraft;
import edu.hitsz.basic.AbstractFlyingObject;
import edu.hitsz.bullet.BaseBullet;
import edu.hitsz.enemyfactory.*;
import edu.hitsz.menu.ScoreRankMenu;
import edu.hitsz.music.MusicManager;
import edu.hitsz.prop.AbstractProp;
import edu.hitsz.prop.BombProp;
import edu.hitsz.prop.BombPropObserver;
import edu.hitsz.prop.BombPropSubject;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 游戏主面板，游戏启动
 *
 * @author hitsz
 */
public abstract class Game extends JPanel implements BombPropSubject {

    private final ScheduledExecutorService executorService = new ScheduledThreadPoolExecutor(
            8, new BasicThreadFactory.Builder().namingPattern("game-action-%d").daemon(true).build());

    private final HeroAircraft heroAircraft = HeroAircraft.getInstance();

    protected final List<EnemyAircraft> enemyAircrafts = new LinkedList<>();
    private final List<BaseBullet> heroBullets = new LinkedList<>();
    private final List<BaseBullet> enemyBullets = new LinkedList<>();
    private final List<AbstractProp> props = new LinkedList<>();
    protected final List<BombPropObserver> bombPropObservers = new LinkedList<>();

    protected final EnemyFactory mobEnemyFactory = new MobEnemyFactory();
    protected final EnemyFactory eliteEnemyFactory = new EliteEnemyFactory();
    protected final EnemyFactory bossEnemyFactory = new BossEnemyFactory();
    protected final EnemyFactory elitePlusEnemyFactory = new ElitePlusEnemyFactory();

    private final int gameDifficultyMode;
    /**
     * 时间间隔(ms)，控制刷新频率
     */
    protected final int timeInterval = 40; // 间隔大就变成慢动作啦，间隔小子弹还没打出来敌机就撞过来了
    private int heroShootCycleTime = 0;
    private int enemyShootCycleTime = 0;
    protected int increaseGameDifficultyCycleTime = 0;
    protected final int increaseGameDifficultyCycleDuration;
    protected int maxEnemyNum = 5; // 屏幕中出现的敌机最大数量
    protected int createEnemyCycleTime = 0;
    protected int createEnemyCycleDuration = 600;
    protected float createEliteEnemyProbability = 0.3f;
    protected int superEliteAircraftCycleTime = 0; // 超级精英敌机的周期计时器
    protected int enemyShootCycleDurantion = 600;
    protected int numOfBossCreated = 0; // boss敌机出现的次数
    protected boolean isBossExist = false; // 当前游戏中是否有Boss敌机
    protected int score = 0; // 当前得分
    private int backGroundTop = 0;

    public Game(boolean musicOn, int gameDifficultyMode, int increaseGameDifficultyCycleDuration) {
        this.gameDifficultyMode = gameDifficultyMode;
        this.increaseGameDifficultyCycleDuration = increaseGameDifficultyCycleDuration;
        MusicManager.setMusicOn(musicOn);
        //启动英雄机鼠标监听
        new HeroController(this, heroAircraft);
    }

    @Override
    public void addBombPropObserver(BombPropObserver bombPropObserver) {
        bombPropObservers.add(bombPropObserver);
    }

    @Override
    public void removeBombPropObserver(BombPropObserver bombPropObserver) {
        bombPropObservers.remove(bombPropObserver);
    }

    @Override
    public void notifyBombPropObservers() {
        for (BombPropObserver observer : bombPropObservers) {
            observer.bombPropNotified();
            if (observer instanceof EnemyAircraft) {
                score += ((EnemyAircraft) observer).getScore();
            }
        }
    }

    /**
     * 游戏启动入口，执行游戏逻辑
     */
    public final void action() {
        MusicManager.playBgm();
        // 定时任务：绘制、对象产生、碰撞判定、击毁及结束判定
        Runnable task = () -> {
            // 增长游戏难度
            increaseGameDifficulty();
            // 产生新敌机
            createEnemyAction();
            // 英雄机射击
            heroShootAction();
            // 敌机射击
            enemyShootAction();
            // 子弹移动
            bulletsMoveAction();
            // 飞机移动
            aircraftsMoveAction();
            // 道具移动
            propsMoveAction();
            // 撞击检测
            crashCheckAction();
            // 后处理
            postProcessAction();
            // 每个时刻重绘界面
            repaint();
            // 英雄机死亡，即游戏结束
            if (heroAircraft.notValid()) {
                MusicManager.overBgm();
                MusicManager.overBossBgm();
                MusicManager.playOverBgm();
                ScoreRankMenu scoreRankMenu = new ScoreRankMenu(gameDifficultyMode);
                Main.cardPanel.add(scoreRankMenu.getMainPanel());
                Main.cardLayout.last(Main.cardPanel);
                scoreRankMenu.inputUser(score);
                executorService.shutdown();
            }
        };
        /*
          以固定延迟时间进行执行
          本次任务执行完成后，需要延迟设定的延迟时间，才会执行新的任务
         */
        executorService.scheduleWithFixedDelay(task, timeInterval, timeInterval, TimeUnit.MILLISECONDS);
    }

    protected abstract void increaseGameDifficulty();

    protected abstract void createEnemyAction();

    // 英雄机射击
    private void heroShootAction() {
        heroShootCycleTime += timeInterval;
        if (heroShootCycleTime >= 600) {
            heroShootCycleTime %= 600;
            heroBullets.addAll(HeroAircraft.getInstance().shoot());
        }
    }

    // 敌机射击
    private void enemyShootAction() {
        enemyShootCycleTime += timeInterval;
        if (enemyShootCycleTime >= enemyShootCycleDurantion) {
            enemyShootCycleTime %= enemyShootCycleDurantion;
            for (EnemyAircraft enemyAircraft : enemyAircrafts) {
                List<BaseBullet> bullets = enemyAircraft.shoot();
                enemyBullets.addAll(bullets);
                for (BaseBullet bullet : bullets) {
                    addBombPropObserver((BombPropObserver) bullet);
                }
            }
        }
    }

    // 子弹移动
    private void bulletsMoveAction() {
        for (BaseBullet bullet : heroBullets) {
            bullet.forward();
        }
        for (BaseBullet bullet : enemyBullets) {
            bullet.forward();
        }
    }

    // 敌机移动
    private void aircraftsMoveAction() {
        for (AbstractAircraft enemyAircraft : enemyAircrafts) {
            enemyAircraft.forward();
        }
    }

    // 道具移动
    private void propsMoveAction() {
        for (AbstractProp prop : props) {
            prop.forward();
        }
    }

    /**
     * 碰撞检测：
     * 1. 敌机攻击英雄
     * 2. 英雄攻击/撞击敌机
     * 3. 英雄获得补给
     */
    private void crashCheckAction() {
        // 敌机子弹攻击英雄
        for (BaseBullet bullet : enemyBullets) {
            if (bullet.notValid()) {
                continue;
            }
            // 英雄机撞击到敌机子弹，英雄机损失一定生命值
            if (bullet.crash(heroAircraft)) {
                heroAircraft.decreaseHp(bullet.getPower()); // 减少子弹的能量
                bullet.vanish();
                MusicManager.playBulletHit();
                if (heroAircraft.notValid()) {
                    return;
                }
            }
        }
        // 英雄机与敌机相撞，均损毁
        for (AbstractAircraft enemyAircraft : enemyAircrafts) {
            if (enemyAircraft.crash(heroAircraft)) {
                enemyAircraft.vanish();
                heroAircraft.vanish();
                return;
            }
        }
        for (BaseBullet bullet : heroBullets) {
            if (bullet.notValid()) {
                continue;
            }
            for (EnemyAircraft enemyAircraft : enemyAircrafts) {
                if (enemyAircraft.notValid()) {
                    // 已被其他子弹击毁的敌机，不再检测
                    // 避免多个子弹重复击毁同一敌机的判定
                    continue;
                }
                // 敌机撞击到英雄机子弹，敌机损失一定生命值
                if (enemyAircraft.crash(bullet)) {
                    enemyAircraft.decreaseHp(bullet.getPower());
                    bullet.vanish();
                    MusicManager.playBulletHit();
                    // 敌机损毁，玩家获得分数，产生道具补给
                    if (enemyAircraft.notValid()) {
                        if (enemyAircraft instanceof BossEnemy) {
                            MusicManager.overBossBgm();
                            MusicManager.playBgm();
                        }
                        score += enemyAircraft.getScore();
                        props.addAll(enemyAircraft.createProps());
                    }
                }
            }
        }
        // 我方获得道具，道具生效
        for (AbstractProp prop : props) {
            if (prop.crash(heroAircraft)) {
                prop.effect();
                if (prop instanceof BombProp) { // 观察者模式
                    notifyBombPropObservers();
                    bombPropObservers.clear();
                }
            }
        }
    }

    /**
     * 后处理：
     * 1. 删除无效的子弹
     * 2. 删除无效的敌机
     * <p>
     * 无效的原因可能是撞击或者飞出边界
     */
    private void postProcessAction() {
        for (BaseBullet bullet : enemyBullets) {
            if (bullet.notValid()) {
                removeBombPropObserver((BombPropObserver) bullet);
            }
        }
        for (EnemyAircraft enemyAircraft : enemyAircrafts) {
            if (enemyAircraft.notValid()) {
                removeBombPropObserver(enemyAircraft);
            }
        }
        enemyBullets.removeIf(AbstractFlyingObject::notValid);
        heroBullets.removeIf(AbstractFlyingObject::notValid);
        enemyAircrafts.removeIf(AbstractFlyingObject::notValid);
        props.removeIf(AbstractFlyingObject::notValid);

    }

    /**
     * 重写paint方法
     * 通过重复调用paint方法，实现游戏动画
     */
    @Override
    public void paint(Graphics g) {
        super.paint(g);
        // 绘制背景,图片滚动
        g.drawImage(ImageManager.BACKGROUND_IMAGE, 0,
                backGroundTop - Main.WINDOW_HEIGHT, null);
        g.drawImage(ImageManager.BACKGROUND_IMAGE, 0, backGroundTop, null);
        backGroundTop += 1;
        if (backGroundTop == Main.WINDOW_HEIGHT) {
            backGroundTop = 0;
        }
        // 先绘制子弹，后绘制飞机
        // 这样子弹显示在飞机的下层
        paintImageWithPositionRevised(g, enemyBullets);
        paintImageWithPositionRevised(g, heroBullets);
        paintImageWithPositionRevised(g, props);
        paintImageWithPositionRevised(g, enemyAircrafts);
        g.drawImage(ImageManager.HERO_IMAGE,
                heroAircraft.getLocationX() - ImageManager.HERO_IMAGE.getWidth() / 2,
                heroAircraft.getLocationY() - ImageManager.HERO_IMAGE.getHeight() / 2
                , null);
        //绘制得分和生命值
        paintScoreAndLife(g);
    }

    private void paintImageWithPositionRevised(Graphics g, List<?
            extends AbstractFlyingObject> objects) {
        if (objects.isEmpty()) {
            return;
        }
        for (AbstractFlyingObject object : objects) {
            BufferedImage image = object.getImage();
            assert image != null : objects.getClass().getName() + " has no image! ";
            g.drawImage(image, object.getLocationX() - image.getWidth() / 2,
                    object.getLocationY() - image.getHeight() / 2, null);
        }
    }

    private void paintScoreAndLife(Graphics g) {
        int x = 10;
        int y = 25;
        g.setColor(new Color(16711680));
        g.setFont(new Font("SansSerif", Font.BOLD, 22));
        g.drawString("SCORE:" + this.score, x, y);
        g.drawString("LIFE:" + this.heroAircraft.getHp(), x, y + 20);
    }
}
