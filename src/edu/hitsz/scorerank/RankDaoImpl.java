package edu.hitsz.scorerank;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class RankDaoImpl implements RankDao {
    private final List<Rank> ranks = new ArrayList<>();
    private final String kSplitString = ",";
    private final String csvPath;

    public RankDaoImpl(int gameDifficulty) {
        csvPath = "./src/edu/hitsz/scorerank/data" + gameDifficulty + ".csv";
        readCsv();
    }

    @Override
    public List<Rank> getAllRanks() {
        return ranks;
    }

    @Override
    public void doAdd(Rank rank) {
        ranks.add(rank);
        doSort();
        writeCsv();
    }

    @Override
    public void doDelete(int rankIndex) {
        ranks.remove(rankIndex);
        doSort();
        writeCsv();
    }

    @Override
    public void doSort() {
        ranks.sort(Comparator.comparingInt(Rank::getScore).reversed());
    }

    @Override
    public void printData() {
        if (ranks.isEmpty()) {
            return;
        }
        System.out.println("""
                **************************************
                               得分排行榜
                **************************************""");
        int i = 1;
        for (Rank rank : ranks) {
            System.out.println("第" + i + "名： " + rank.getUserName() + ", " + rank.getScore() + ", " + rank.getTime());
            i++;
        }
    }

    private void readCsv() {
        try {
            BufferedReader br = new BufferedReader(new FileReader(csvPath));
            String line;
            while ((line = br.readLine()) != null) {
                String[] data = line.split(kSplitString); // 使用逗号分隔每一行数据，并存储到数组中
                ranks.add(new Rank(data[0], Integer.parseInt(data[1].trim()), data[2].trim())); // 创建Rank对象并加入到ranks列表
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeCsv() {
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(csvPath);
            for (Rank rank : ranks) {
                String s = rank.getUserName() + kSplitString + rank.getScore() + kSplitString + rank.getTime() + "\n";
                fos.write(s.getBytes());
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            if (fos != null) try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
