package edu.hitsz.scorerank;

import java.util.List;

public interface RankDao {
    List<Rank> getAllRanks();

    void doAdd(Rank rank);

    void doDelete(int rankIndex);

    void doSort();

    void printData();
}
