package edu.hitsz.scorerank;

public class Rank {

    private final String userName;
    private final int score;
    private final String time;

    public Rank(String userName, int score, String time) {
        this.userName = userName;
        this.score = score;
        this.time = time;
    }

    public String getUserName() {
        return userName;
    }

    public int getScore() {
        return score;
    }

    public String getTime() {
        return time;
    }
}
