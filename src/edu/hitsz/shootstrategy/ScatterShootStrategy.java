package edu.hitsz.shootstrategy;

import edu.hitsz.aircraft.AbstractAircraft;
import edu.hitsz.aircraft.HeroAircraft;
import edu.hitsz.bullet.BaseBullet;
import edu.hitsz.bullet.EnemyBullet;
import edu.hitsz.bullet.HeroBullet;

import java.util.LinkedList;
import java.util.List;

public class ScatterShootStrategy implements ShootStrategy {
    private int shootCycle = 2;

    @Override
    public List<BaseBullet> shoot(AbstractAircraft aircraft) {
        List<BaseBullet> res = new LinkedList<>();
        int x = aircraft.getLocationX();
        int y = aircraft.getLocationY() + aircraft.getDirection() * 2;
        int speedY = aircraft.getSpeedY() + aircraft.getDirection() * 5;
        BaseBullet bullet;
        if (aircraft instanceof HeroAircraft) {
            for (int i = 0; i < aircraft.getShootNum(); i++) {
                // 子弹发射位置相对飞机位置向前偏移
                // 多个子弹横向分散
                bullet = new HeroBullet(x + (i * 2 - aircraft.getShootNum() + 1) * 10, y, (i * 2 - aircraft.getShootNum() + 1), speedY, aircraft.getBulletPower());
                res.add(bullet);
            }
        } else {
            if (shootCycle == 2) {
                for (int i = 0; i < aircraft.getShootNum(); i++) {
                    bullet = new EnemyBullet(x + (i * 2 - aircraft.getShootNum() + 1) * 10, y, (i * 2 - aircraft.getShootNum() + 1), speedY, aircraft.getBulletPower());
                    res.add(bullet);
                }
                shootCycle = 1;
            } else {
                shootCycle += 1;
            }
        }
        return res;
    }
}
