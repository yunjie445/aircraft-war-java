package edu.hitsz.shootstrategy;

import edu.hitsz.aircraft.AbstractAircraft;
import edu.hitsz.aircraft.HeroAircraft;
import edu.hitsz.bullet.BaseBullet;
import edu.hitsz.bullet.EnemyBullet;
import edu.hitsz.bullet.HeroBullet;

import java.util.LinkedList;
import java.util.List;

public class CircleShootStrategy implements ShootStrategy {
    private int shootCycle = 2;

    @Override
    public List<BaseBullet> shoot(AbstractAircraft aircraft) {
        List<BaseBullet> res = new LinkedList<>();
        BaseBullet bullet;
        double angleInterval = 2 * Math.PI / aircraft.getShootNum(); // 分布在360度圆周上的角度间隔
        if (aircraft instanceof HeroAircraft) {
            for (int i = 0; i < aircraft.getShootNum(); i++) {
                double angle = i * angleInterval;
                int x = aircraft.getLocationX() + (int) Math.cos(angle);
                int y = aircraft.getLocationY() + (int) Math.sin(angle);
                int speedX = (int) (Math.cos(angle) * 5); // 每个子弹的速度方向和大小不同
                int speedY = (int) (Math.sin(angle) * 5); // 每个子弹的速度方向和大小不同
                bullet = new HeroBullet(x, y, speedX, speedY, aircraft.getBulletPower());
                res.add(bullet);
            }
        } else {
            if (shootCycle == 2) {
                for (int i = 0; i < aircraft.getShootNum(); i++) {
                    double angle = i * angleInterval;
                    int x = aircraft.getLocationX() + (int) Math.cos(angle);
                    int y = aircraft.getLocationY() + (int) Math.sin(angle);
                    ;
                    int speedX = (int) (Math.cos(angle) * 5); // 每个子弹的速度方向和大小不同
                    int speedY = (int) (Math.sin(angle) * 5); // 每个子弹的速度方向和大小不同
                    bullet = new EnemyBullet(x, y, speedX, speedY, aircraft.getBulletPower());
                    res.add(bullet);
                }
                shootCycle = 1;
            } else {
                shootCycle += 1;
            }
        }

        return res;
    }
}
