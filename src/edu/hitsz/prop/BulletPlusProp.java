package edu.hitsz.prop;

import edu.hitsz.aircraft.HeroAircraft;
import edu.hitsz.music.MusicManager;
import edu.hitsz.shootstrategy.CircleShootStrategy;
import edu.hitsz.shootstrategy.DirectShootStrategy;

public class BulletPlusProp extends AbstractProp {
    public BulletPlusProp(int locationX, int locationY, int speedX, int speedY) {
        super(locationX, locationY, speedX, speedY);
    }

    @Override
    public void effect() {
        MusicManager.playBullet();
        System.out.println("Fire Supply active!");
        //定义新线程
        Runnable r = () -> {
            HeroAircraft.getInstance().setShootNum(10);
            HeroAircraft.getInstance().setShootStrategy(new CircleShootStrategy());
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            HeroAircraft.getInstance().setShootNum(1);
            HeroAircraft.getInstance().setShootStrategy(new DirectShootStrategy());
        };
        new Thread(r).start();
        vanish();
    }
}
