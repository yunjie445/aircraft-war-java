package edu.hitsz.prop;

import edu.hitsz.aircraft.HeroAircraft;
import edu.hitsz.music.MusicManager;
import edu.hitsz.shootstrategy.DirectShootStrategy;
import edu.hitsz.shootstrategy.ScatterShootStrategy;

public class BulletProp extends AbstractProp {

    public BulletProp(int locationX, int locationY, int speedX, int speedY) {
        super(locationX, locationY, speedX, speedY);
    }

    @Override
    public void effect() {
        MusicManager.playBullet();
        System.out.println("Fire Supply active!");
        //定义新线程
        Runnable r = () -> {
            HeroAircraft.getInstance().setShootNum(3);
            HeroAircraft.getInstance().setShootStrategy(new ScatterShootStrategy());
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            HeroAircraft.getInstance().setShootNum(1);
            HeroAircraft.getInstance().setShootStrategy(new DirectShootStrategy());
        };
        new Thread(r).start();
        vanish();
    }
}
