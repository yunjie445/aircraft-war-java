package edu.hitsz.prop;

public interface BombPropSubject {

    void addBombPropObserver(BombPropObserver bombPropObserver);

    void removeBombPropObserver(BombPropObserver bombPropObserver);

    void notifyBombPropObservers();
}
