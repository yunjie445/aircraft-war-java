package edu.hitsz.prop;

import edu.hitsz.music.MusicManager;

public class BombProp extends AbstractProp {

    public BombProp(int locationX, int locationY, int speedX, int speedY) {
        super(locationX, locationY, speedX, speedY);
    }

    @Override
    public void effect() {
        MusicManager.playBomb();
        System.out.println("Bomb Supply active!");
        vanish();
    }
}
