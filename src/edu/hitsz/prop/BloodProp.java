package edu.hitsz.prop;

import edu.hitsz.aircraft.HeroAircraft;
import edu.hitsz.music.MusicManager;

public class BloodProp extends AbstractProp {

    public BloodProp(int locationX, int locationY, int speedX, int speedY) {
        super(locationX, locationY, speedX, speedY);
    }

    @Override
    public void effect() {
        MusicManager.playSupply();
        HeroAircraft.getInstance().increaseHp(50);
        System.out.println("Blood Supply active!");
        vanish();
    }
}
