package edu.hitsz.prop;

public interface BombPropObserver {

    void bombPropNotified();
}
