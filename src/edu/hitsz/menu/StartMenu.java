package edu.hitsz.menu;

import edu.hitsz.application.*;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.io.FileInputStream;
import java.io.IOException;

public class StartMenu {
    public static final int WINDOW_WIDTH = 512;
    public static final int WINDOW_HEIGHT = 768;
    private JPanel MainPanel;
    private JPanel TopPanel;
    private JPanel BottomPanel;
    private JButton easyButton;
    private JButton mediumButton;
    private JButton hardButton;
    private JLabel musicLable;
    private JComboBox MusicComboBox;

    public StartMenu() {
        easyButton.addActionListener(e -> onClickDifficultyButton(1));
        mediumButton.addActionListener(e -> onClickDifficultyButton(2));
        hardButton.addActionListener(e -> onClickDifficultyButton(3));
    }

    public JPanel getMainPanel() {
        return MainPanel;
    }

    private void onClickDifficultyButton(int gameDifficulty) {
        try {
            ImageManager.BACKGROUND_IMAGE = ImageIO.read(new FileInputStream("src/images/bg" + gameDifficulty + ".jpg"));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        boolean musicOn = MusicComboBox.getSelectedIndex() == 0;
        Game game;
        if (gameDifficulty == 1) {
            System.out.println("选择了简单模式");
            game = new EasyGame(musicOn);
        } else if (gameDifficulty == 2) {
            System.out.println("选择了中等模式");
            game = new MediumGame(musicOn);
        } else if (gameDifficulty == 3) {
            System.out.println("选择了困难模式");
            game = new HardGame(musicOn);
        } else {
            throw new IllegalArgumentException("Invalid game difficulty: " + gameDifficulty);
        }
        game.action();
        Main.cardPanel.add(game);
        Main.cardLayout.last(Main.cardPanel);
    }
}
