package edu.hitsz.menu;

import edu.hitsz.scorerank.Rank;
import edu.hitsz.scorerank.RankDao;
import edu.hitsz.scorerank.RankDaoImpl;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ScoreRankMenu {
    public static final int WINDOW_WIDTH = 512;
    public static final int WINDOW_HEIGHT = 768;
    private final RankDao rankDao;
    String[] columnName = {"名次", "玩家名", "得分", "游戏结束时间"};
    private JPanel topPanel;
    private JButton deleteButton;
    private JScrollPane tableScrollPane;
    private JLabel rankingLable;
    private JTable scoreTable;
    private JLabel levelLabel;
    private JPanel mainPanel;
    private JPanel bottomPanel;

    public ScoreRankMenu(int gameDifficulty) {
        //界面左上角展示文字
        if (gameDifficulty == 1) {
            levelLabel.setText("难度：EASY");
        } else if (gameDifficulty == 2) {
            levelLabel.setText("难度：MEDIUM");
        } else if (gameDifficulty == 3) {
            levelLabel.setText("难度：HARD");
        }
        deleteButton.addActionListener(e -> onClickDeleteButton());
        rankDao = new RankDaoImpl(gameDifficulty);
        refreshScoreTable();
    }

    public void inputUser(int score) {
        // 弹出一个带有输入框的对话框
        String input = JOptionPane.showInputDialog(null, "游戏结束，您的分数为" + score + "\n" + "请输入名字记录得分：");
        if (input == null || input.isEmpty()) {
            JOptionPane.showMessageDialog(null, "未成功保存数据", "warning", JOptionPane.WARNING_MESSAGE);
            rankDao.printData();
        } else {
            // 获取当前时间
            Date date = new Date();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            // 记录数据
            rankDao.doAdd(new Rank(input, score, formatter.format(date)));
            refreshScoreTable();
        }
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }

    private void refreshScoreTable() {
        List<Rank> rankList = rankDao.getAllRanks();
        int userNum = rankList.size();
        String[][] tableData = new String[userNum][4];
        for (int i = 0; i < userNum; i++) {
            Rank user = rankList.get(i);
            tableData[i][0] = (i + 1) + "";
            tableData[i][1] = user.getUserName();
            tableData[i][2] = user.getScore() + "";
            tableData[i][3] = user.getTime();
        }
        DefaultTableModel model = new DefaultTableModel(tableData, columnName) {
            @Override
            //用户不能编辑
            public boolean isCellEditable(int row, int col) {
                return false;
            }
        };
        //JTable并不存储自己的数据，而是从表格模型那里获取它的数据
        scoreTable.setModel(model);
        tableScrollPane.setViewportView(scoreTable);
    }

    private void onClickDeleteButton() {
        int row = scoreTable.getSelectedRow();
        if (row != -1) {
            int result = JOptionPane.showConfirmDialog(deleteButton, "是否确定中删除？");
            if (JOptionPane.YES_OPTION == result) {
                rankDao.doDelete(row);
            }
        } else {
            JOptionPane.showMessageDialog(null, "没有选中删除选项", "warning", JOptionPane.WARNING_MESSAGE);
        }
        refreshScoreTable();
    }
}
