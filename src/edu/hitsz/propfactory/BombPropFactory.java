package edu.hitsz.propfactory;

import edu.hitsz.prop.AbstractProp;
import edu.hitsz.prop.BombProp;

public class BombPropFactory extends PropFactory {
    @Override
    public AbstractProp creatProp(int locationX, int locationY) {
        return new BombProp(locationX, locationY, 0, 6);
    }
}
