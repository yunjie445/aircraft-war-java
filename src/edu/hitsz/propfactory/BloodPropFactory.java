package edu.hitsz.propfactory;

import edu.hitsz.prop.AbstractProp;
import edu.hitsz.prop.BloodProp;

public class BloodPropFactory extends PropFactory {
    @Override
    public AbstractProp creatProp(int locationX, int locationY) {
        return new BloodProp(locationX, locationY, 0, 6);
    }
}
