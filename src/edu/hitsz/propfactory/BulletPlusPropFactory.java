package edu.hitsz.propfactory;

import edu.hitsz.prop.AbstractProp;
import edu.hitsz.prop.BulletPlusProp;

public class BulletPlusPropFactory extends PropFactory {
    public AbstractProp creatProp(int locationX, int locationY) {
        return new BulletPlusProp(locationX, locationY, 0, 6);
    }
}
