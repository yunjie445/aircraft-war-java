package edu.hitsz.propfactory;

import edu.hitsz.prop.AbstractProp;
import edu.hitsz.prop.BulletProp;

public class BulletPropFactory extends PropFactory {
    @Override
    public AbstractProp creatProp(int locationX, int locationY) {
        return new BulletProp(locationX, locationY, 0, 6);
    }
}
