package edu.hitsz.propfactory;

import edu.hitsz.prop.AbstractProp;

public abstract class PropFactory {
    public abstract AbstractProp creatProp(int locationX, int locationY);
}
