package edu.hitsz.aircraft;

import edu.hitsz.shootstrategy.DirectShootStrategy;


/**
 * 精英敌机
 * 可射击
 *
 * @author hitsz
 */
public class EliteEnemy extends EnemyAircraft {

    public EliteEnemy(int locationX, int locationY, int speedX, int speedY, int hp) {
        super(locationX, locationY, speedX, speedY, hp);
        this.shootNum = 1;
        this.bulletPower = 30;
        this.shootStrategy = new DirectShootStrategy();
        this.score = 30;
        this.propNum = 1;
    }
}
