package edu.hitsz.aircraft;

import edu.hitsz.shootstrategy.CircleShootStrategy;

public class BossEnemy extends EnemyAircraft {

    public BossEnemy(int locationX, int locationY, int speedX, int speedY, int hp) {
        super(locationX, locationY, speedX, speedY, hp);
        this.shootNum = 20;
        this.bulletPower = 50;
        this.shootStrategy = new CircleShootStrategy();
        this.score = 70;
        this.propNum = 3;
    }

    @Override
    public void bombPropNotified() {
        // 炸弹道具对boss不影响，这里重写为空
    }
}
