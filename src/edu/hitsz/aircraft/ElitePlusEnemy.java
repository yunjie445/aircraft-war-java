package edu.hitsz.aircraft;

import edu.hitsz.shootstrategy.ScatterShootStrategy;

public class ElitePlusEnemy extends EnemyAircraft{
    public ElitePlusEnemy(int locationX, int locationY, int speedX, int speedY, int hp) {
        super(locationX, locationY, speedX, speedY, hp);
        this.shootNum = 3;
        this.bulletPower = 30;
        this.shootStrategy = new ScatterShootStrategy();
        this.score = 50;
        this.propNum = 1;
    }
}
