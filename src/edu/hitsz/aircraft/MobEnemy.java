package edu.hitsz.aircraft;

import edu.hitsz.shootstrategy.DirectShootStrategy;

/**
 * 普通敌机
 * 不可射击
 *
 * @author hitsz
 */
public class MobEnemy extends EnemyAircraft {

    public MobEnemy(int locationX, int locationY, int speedX, int speedY, int hp) {
        super(locationX, locationY, speedX, speedY, hp);
        this.shootNum = 0;
        this.shootStrategy = new DirectShootStrategy(); // 虽然mob不可射击，但为了在game中统一，这里仍然设置一个策略
        this.score = 50;
        this.propNum = 0;
    }
}
