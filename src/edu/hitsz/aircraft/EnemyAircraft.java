package edu.hitsz.aircraft;

import edu.hitsz.application.Main;
import edu.hitsz.prop.AbstractProp;
import edu.hitsz.prop.BombPropObserver;
import edu.hitsz.propfactory.BloodPropFactory;
import edu.hitsz.propfactory.BombPropFactory;
import edu.hitsz.propfactory.BulletPropFactory;
import edu.hitsz.propfactory.PropFactory;

import java.util.LinkedList;

public abstract class EnemyAircraft extends AbstractAircraft implements BombPropObserver {

    protected int score;
    protected int propNum;

    public EnemyAircraft(int locationX, int locationY, int speedX, int speedY, int hp) {
        super(locationX, locationY, speedX, speedY, hp);
        this.direction = 1;
    }

    @Override
    public void bombPropNotified() {
        decreaseHp(30);
    }

    public void forward() {
        super.forward();
        // 判定 y 轴向下飞行出界
        if (locationY >= Main.WINDOW_HEIGHT) {
            vanish();
        }
    }

    public LinkedList<AbstractProp> createProps() {
        LinkedList<AbstractProp> res = new LinkedList<>();
        for (int i = 0; i < propNum; i++) {
            AbstractProp prop = createSingleProp(this.getLocationX() + (i * 2 - propNum + 1) * 20, this.getLocationY());
            if (prop != null) {
                res.add(prop);
            }
        }
        return res;
    }

    public int getScore() {
        return score;
    }

    private AbstractProp createSingleProp(int locationX, int locationY) {
        double r = Math.random();
        PropFactory propFactory;
        if (r < 0.3) { // 补血道具
            propFactory = new BloodPropFactory();
        } else if (r < 0.6) { // 爆炸道具
            propFactory = new BombPropFactory();
        } else if (r < 0.9) { // 火力道具
            propFactory = new BulletPropFactory();
        } else {
            return null;
        }
        return propFactory.creatProp(locationX, locationY);
    }
}
